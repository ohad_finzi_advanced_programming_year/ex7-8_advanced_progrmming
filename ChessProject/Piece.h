#pragma once

#include <iostream>
#include "Point.h"


#define FILE_AND_RANK 8


class Piece
{

protected:
	//fields
	char _type;
	bool _color;
	Point _pos;

	//relevent only for the pawn piece
	bool _first;  //checks if the Pawn is in the starting positions when he can move two steps forward

public:
	//constructor and deconstuctor
	Piece(const char& type, const Point& pos, const bool first = false);
	virtual ~Piece();

	//getters and setters
	Point getPos() const;
	void set_Pos(const Point& pos);
	bool getColor() const;
	char getType() const;

	void resetFirst();  //restes the _first variable after a move

	//pure abstract function
	virtual bool legalMove(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const = 0;

};


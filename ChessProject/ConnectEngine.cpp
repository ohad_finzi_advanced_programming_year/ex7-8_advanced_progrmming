#include "ConnectEngine.h"


/*
The function constructs the current ConnnectEngine class by connecting to the game engine and sending the starting board
*/
ConnectEngine::ConnectEngine(const std::string& startingBoard):
	_pipe()
{
	this->connect();
	this->sendMsg(startingBoard);  //sends the first msg to the engine
}


/*
The function deconstructs the current ConnectEngine class by closing the pipe to the game engine
*/
ConnectEngine::~ConnectEngine()
{
	_pipe.close();
}


/*
The function check if the connection to the engine is valid. incase the connection isnt valid it asks the user if he wants to keep trying to connect to the game engine
*/
void ConnectEngine::connect()
{
	std::string ans;

	while (!_pipe.connect())
	{
		std:: cout << "cant connect to graphics" << std::endl;
		std::cout << "Do you want to try and connect again or exit? (0-try again, any other input-exit)" << std::endl;
		std::getline(std::cin, ans);

		if (ans == "0")  //if the user wants to keep trying to connect to the game engine
		{
			std::cout << "trying to connect again.." << std::endl;
			Sleep(5000);  //stops for a few seconds before trying to connect again   
		}  
		else  //if the user wants to stop trying to connect to the game engine
		{
			throw "";  //decontructs the current class and returns to main
		}
	}
}


/*
The function sends the given msg to the connected game engine
Input: the wanted msg to pass to the game engine
*/
void ConnectEngine::sendMsg(const std::string& msg)
{
	char msgToGraphics[MAX_MSG_CHARS];
	strcpy_s(msgToGraphics, msg.c_str());
	_pipe.sendMessageToGraphics(msgToGraphics);
}


/*
The function receives an answer from the game engine
Output: the given answer from the server
*/
std::string ConnectEngine::receiveMsg()
{
	return _pipe.getMessageFromGraphics();
}


#pragma  once

#include "Queen.h"


/*
The functions constructs the current Queen class according to the given elements
Input: the type of the Piece ('q' or 'Q'), its position on the board, if the piece can move in diagonal lines, if the piece can move in straight lines and if the piece is limites to only one step
*/
Queen::Queen(const char type, const Point& pos, const bool diagonal, const bool straight, const bool limit):
	Piece(type, pos), _diagonal(diagonal), _straight(straight), _limit(limit)
{}


/*
The function deconstructs the current Queen class
*/
Queen::~Queen()
{
	//no dynamic memory alocated in this class
}


/*
The function overrides the pure abstract function from Piece class.
The function checks if the given string movement is valid according to the current piece.
Input: the wanted movement of the current piece 
Output: true if the move is valid according to the current piece and false if not
*/
bool Queen::legalMove(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const
{
	bool ans = false;

	if (this->_diagonal && !ans)  //the condition checks if the movement is in a diagonal line if the current piece can move in diagonal lines
	{
		ans = checkDiagonal(dstPos, board);
	}
	if (this->_straight && !ans)  //the condition checks if the movement is in a straight line if the current piece can move in straight lines
	{
		ans = checkStraight(dstPos, board);
	}
	if (this->_limit)  //the condition checks if the movement is only one step, if the current piece is limited to only one step at a time
	{
		ans = (this->_pos.calcDis(dstPos) == 1) ? true : false;  
	}

	return ans;
}


/*
The function checks if the given diagonal movement is valid. it checks if the movement is actually a diagonal line and checks if no player is blocking the movement
Input: the wanted dst position and the board with all of the pieces
Output: true if the move is valid and false if not
*/
bool Queen::checkDiagonal(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const
{
	bool ans = false;
	int srcI = _pos.getX();
	int srcJ = _pos.getY();
	int dstI = dstPos.getX();
	int dstJ = dstPos.getY();
	int i = 0;
	int j = 0;

	if (abs(srcI - dstI) == abs(srcJ - dstJ))  //checks if the movement is in a diagonal line
	{
		ans = true;

		//each condition checks a different diagonal line direction, to see if there is any piece which can block the wanted movement
		if (srcI < dstI)  //checks the diagonal lines on the right side
		{
			if (srcJ < dstJ)  //checks the upper diagonal line
			{
				for (i = srcI + 1, j = srcJ + 1; i < dstI && j < dstJ && ans; i++, j++)
				{
					if (board[i][j] != nullptr)  //checks if the current position there is any other piece which can block the wanted movement
					{
						ans = false;
					}
				}
			}
			else  //checks the lower diagonal line
			{
				for (i = srcI + 1, j = srcJ - 1; i < dstI && j > dstJ && ans; i++, j--)
				{
					if (board[i][j] != nullptr)
					{
						ans = false;
					}
				}
			}
		}
		else if (srcI > dstI)  //checks the diagonal lines on the left side
		{
			if (srcJ < dstJ)  //checks the upper diagonal line
			{
				for (i = srcI - 1, j = srcJ + 1; i > dstI && j < dstJ && ans; i--, j++)
				{
					if (board[i][j] != nullptr)
					{
						ans = false;
					}
				}
			}
			else  //checks the lower diagonal line
			{
				for (i = srcI - 1, j = srcJ - 1; i > dstI && j > dstJ && ans; i--, j--)
				{
					if (board[i][j] != nullptr)
					{
						ans = false;
					}
				}
			}
		}
	}

	return ans;
}


/*
The function checks if the given straight movement is valid. it checks if the movement is actually a straight line and checks if no player is blocking the movement
Input: the wanted dst position and the board with all of the pieces
Output: true if the move is valid and false if not
*/
bool Queen::checkStraight(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const
{
	bool ans = false;
	int srcI = _pos.getX();
	int srcJ = _pos.getY();
	int dstI = dstPos.getX();
	int dstJ = dstPos.getY();
	int i = 0;
	int j = 0;

	if ((srcI == dstI) ^ (srcJ == dstJ))  //checks if the movement is in a striaght line
	{
		ans = true;

		if (srcI == dstI)  //in a vertical line
		{
			if (srcJ < dstJ)  //the wanted movement is verticly upwards
			{
				for (j = srcJ + 1; j < dstJ && ans; j++)
				{
					if (board[srcI][j] != nullptr)
					{
						ans = false;
					}
				}
			}
			else  //the wanted movement is verticly downwards
			{
				for (j = srcJ - 1; j > dstJ && ans; j--)
				{
					if (board[srcI][j] != nullptr)
					{
						ans = false;
					}
				}
			}
		}
		else if (srcJ == dstJ)  //in a horizontal line
		{
			if (srcI < dstI)  //the wanted movement is horizontly to the right
			{
				for (i = srcI + 1; i < dstI && ans; i++)
				{
					if (board[i][srcJ] != nullptr)
					{
						ans = false;
					}
				}
			}
			else  ////the wanted movement is horizontly to the left
			{
				for (i = srcI - 1; i > dstI && ans; i--)
				{
					if (board[i][srcJ] != nullptr)
					{
						ans = false;
					}
				}
			}
		}
	}

	return ans;
}


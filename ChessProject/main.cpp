#include <iostream>
#include "ControlPanel.h"


int main()
{
	try
	{
		ControlPanel chessGame = ControlPanel();
		chessGame.runGame();
	}
	catch (...)
	{
		//the "throw" got activated in ConnectEngine which means the user dont want to connect to the engine once again
	}

	std::cout << "BYEBYE" << std::endl;

	return 0;
}


#include "Point.h"


/*
The functions constructs the current point class if there are no given elements
*/
Point::Point():
	_x(0), _y(0)
{
}


/*
The functions constructs the current point class according to the given element
Input: x and y ints of the current point
*/
Point::Point(const int x, const int y):
	_x(x), _y(y)
{
}


/*
The functions constructs the current point class according to the given element
Input: pos of the piece on the board. needs to be converted to ints
*/
Point::Point(const std::string& pos)
{
	this->setPos(pos);
}


/*
The functions constructs the current point class according to the given element
Input: a new point to copy its x and y elemets
*/
Point::Point(const Point& p)
{
	*this = p;
}


/*
The functions deconstructs the current Point class
*/
Point::~Point()
{
	//no dynamic memory alocated in this class
}


/*
The function returns the x value of the current class
*/
int Point::getX() const
{
	return this->_x;
}


/*
The function returns the y value of the current class
*/
int Point::getY() const
{
	return this->_y;
}


/*
The function sets a new x of the current point class according to the given element
*/
void Point::setX(const int x)
{
	this->_x = x;
}


/*
The function sets a new y of the current point class according to the given element
*/
void Point::setY(const int y)
{
	this->_y = y;
}


/*
The functions sets a new x and y to the current point class accoring to the given piece position on the board
*/
void Point::setPos(const std::string& pos)
{
	this->_x = pos[1] - FIRST_NUMBER;
	this->_y = pos[0] - FIRST_LOWER;
}


/*
The functions calculates the distance between the current point class and the given point class
Input: another point class
Output: the distance betweeen the two points
*/
int Point::calcDis(const Point& p) const
{
	return sqrt(pow(this->_x - p.getX(), 2) + pow(this->_y - p.getY(), 2)) * 1.0;
}


/*
The functiosn ovverides the += operator and uses it the same way but with the Point class elements
Input: a Point class to add to the current Point class
Output: the sum of the current Point class and the given Point class
*/
Point& Point::operator+=(const Point& p)
{
	this->_x += p.getX();
	this->_y += p.getY();

	return *this;
}


/*
The functions overrides the = operators and uses it the same way but with the Point class elements
Input: a Point class to put instead of the current Point class
Output: the new Point class
*/
Point& Point::operator=(const Point& p)
{
	if (this == &p)
	{
		return *this;
	}

	this->_x = p.getX();
	this->_y = p.getY();

	return *this;
}


/*
The function overrides the == operator and uses it the same way but with the Point class elements
Input: a Point class to check if it equals to the current Point class
Output: true if equals and false if not
*/
bool Point::operator==(const Point& p) const
{
	return this->_y == p.getY() && this->_x == p.getX();
}


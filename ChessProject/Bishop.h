#pragma once

#include "Queen.h"


class Bishop : public Queen
{

public:
	//constructor and deconstructor
	Bishop(const char type, const Point& pos) :
		Queen(type, pos, true, false, false)  //the Bishop can move in diagonal lines and he is not limited
	{
	}
	virtual ~Bishop()
	{
		//no dynamic memory alocated in this class
	}

};


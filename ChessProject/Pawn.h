#pragma once

#include "Piece.h"


class Pawn : public Piece
{

public:
	//constructor and deconstructor
	Pawn(const char type, const Point pos);
	virtual ~Pawn();

	//override the pure abstract function move from Piece class
	virtual bool legalMove(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const;  //do not forget: changes the _first variable to be false after the first move

};


#include "Board.h"


/*
The function constructs the current Board class by creating a 2d array which holds all the pieces appears in the engine
Input: the string which symbolizes all the board's pieces
*/
Board::Board(std::string board)
{
	int i = 0;
	int j = 0;
	int counter = 0;
	std::string pos = "";
	Point currPos = Point();

	//the loop creates each piece and puts it in the board (starts from the last places to fit with the engine)
	for (i = FILE_AND_RANK - 1; i >= 0; i--)
	{
		for (j = 0; j < FILE_AND_RANK; j++, counter++)
		{
			currPos.setX(i);
			currPos.setY(j);

			//the switch creates each according to the current piece from the string
			switch (tolower(board[counter]))
			{
			case 'k':
				this->_board[i][j] = new King(board[counter], currPos);
				break;

			case 'q':
				this->_board[i][j] = new Queen(board[counter], currPos);
				break;

			case 'r':
				this->_board[i][j] = new Rook(board[counter], currPos);
				break;

			case 'n':
				this->_board[i][j] = new Knight(board[counter], currPos);
				break;

			case 'b':
				this->_board[i][j] = new Bishop(board[counter], currPos);
				break;

			case 'p':
				this->_board[i][j] = new Pawn(board[counter], currPos);
				break;

			default:
				this->_board[i][j] = nullptr;
				break;
			}
		}
	}
}


/*
The functiosn deconstructs all the allocated dynamic memory allocated for the current class
*/
Board::~Board()
{
	int i = 0;
	int j = 0;

	//runs through the 2d array and deletes each pointer to a Piece class
	for (i = 0; i < FILE_AND_RANK; i++)
	{
		for (j = 0; j < FILE_AND_RANK; j++)
		{
			delete this->_board[i][j];
		}
	}
}


/*
The functiosn checks the given movement according to the current turn
Input: the movment - Point class src which symbolizes the piece the user want to move, Point class dst which symbolizes the place the user wants to put the src piece in. and the current turn (black - true, white - false)
Output: an int number which symbolizes if the given movement is valid or not according to the enigne protocol
*/
int Board::checkMove(const Point& src, const Point& dst, bool currTurn) 
{
	int ans = VALID;
	int srcJ = src.getY();
	int srcI = src.getX();
	int dstJ = dst.getY();
	int dstI = dst.getX();
	bool isChess = false;

	if (srcI < 0 || srcI >= FILE_AND_RANK || srcJ < 0 || srcJ >= FILE_AND_RANK || dstI < 0 || dstI >= FILE_AND_RANK || dstJ < 0 || dstJ >= FILE_AND_RANK)  //check if all the given indexes are inside the board boundries (between 0 and 8)
	{
		ans = INDEXINVALID;  //return ivalid move 5
	}
	else if (this->_board[srcI][srcJ] == nullptr || this->_board[srcI][srcJ]->getColor() != currTurn)  //check if the src position has a piece which belongs to the current turn side
	{
		ans = SRCINVALID;  //return invalid move 2
	}
	else if (src == dst)  //check if the src position and the dst position arent on the same block
	{
		ans = WRONGINDEX;  //return invalid move 7
	}
	else if (this->_board[dstI][dstJ] != nullptr && this->_board[dstI][dstJ]->getColor() == currTurn)  //check if the dst position dont have a piece of the current turn side
	{
		ans = DSTINVALID;  //return invalid move 3
	}
	else
	{
		ans = this->_board[srcI][srcJ]->legalMove(dst, this->_board);  //checks if the move is valid according to the current src piece
		if (ans)
		{
			//check if after the wanted move there is a chess on this king
			isChess = checkChess(src, dst, currTurn);
			if (isChess)  //if chess the current player
			{
				ans = CHESSINVALID;  //return invalid move4
			}
			else  //if not chessing the current player, the move is valid, and now checks if its a regular move, a chess on the other side move, or a checkmate on the other side move
			{
				//check if after the wanted move there is a chess on the other side king
				isChess = checkChess(src, dst, !currTurn);
				if (isChess)
				{
					movePiece(src, dst);  //creating the wanted movement
					isChess = checkMate(!currTurn);  //checks if the wanted movement creates a checkmate
					if (isChess)  //if checkmate
					{
						ans = CHECKMATE;  //returns valid 8
					}
					else  //if not checkmate
					{
						ans = CHESSVALID;  //returns valid 0
					}
				}
				else  //the move is regular but valid
				{
					movePiece(src, dst);  //creating the wanted movement
					ans = VALID;
				}
			}
		}
		else  //the move is not correct for the wanted piece
		{
			ans = MOVEINVALID;  //returns invalid error 6
		}
	}

	return ans;
}


/*
The function checks if the given movement creates a chess on the given turn king.
The function creates a temp board with the given move, finds the given turn king and checks if there is any player who can chess the given turn king.
Input: the wanted movement to make (splitted to dst and src Points class) and the wanted turn to check
Output: true if there is a chess on the king, and false if not
*/
bool Board::checkChess(const Point src, const Point dst, const bool turn)
{
	Piece* tempBoard[FILE_AND_RANK][FILE_AND_RANK] = { 0 };
	Point king = Point();
	Point oldPos = Point();
	Point newPos = Point();
	int i = 0;
	int j = 0;
	bool ans = false;

	//creates a temporary board which contains all the pieces to make a the wanted move on it
	for (i = 0; i < FILE_AND_RANK; i++)
	{
		for (j = 0; j < FILE_AND_RANK; j++)
		{
			tempBoard[i][j] = this->_board[i][j];
		}
	}

	//creates the movement on the temp board
	tempBoard[src.getX()][src.getY()]->set_Pos(dst);
	tempBoard[dst.getX()][dst.getY()] = tempBoard[src.getX()][src.getY()];
	tempBoard[src.getX()][src.getY()] = nullptr;

	//gets the coordinate of the king, inserts them in i and j and created a point which symoblizes the king position
	this->getKing(tempBoard, turn, i, j); 
	king.setX(i);
	king.setY(j);
	
	//runs through the tempBoard pieces
	for (i = 0; i < FILE_AND_RANK && !ans; i++)
	{
		for (j = 0; j < FILE_AND_RANK && !ans; j++)
		{
			//if the current piece is the opposite of the given turn, it checks if it can eat the king by sending the king position to the legal move function
			if (tempBoard[i][j] != nullptr && tempBoard[i][j]->getColor() != turn)
			{
				ans = tempBoard[i][j]->legalMove(king, tempBoard);
			}
		}
	}

	tempBoard[dst.getX()][dst.getY()]->set_Pos(src);  //resetores the position of the src movment piece back to its original place
	return ans;
}


/*
The function checks if there is a checkmate on the given king
Input: the king to check (true - black, false - white)
Output: true for checkmate on the given king color and false for no checkmate
*/
bool Board::checkMate(const bool turn)
{
	bool ans = true;
	int srcI = 0;
	int srcJ = 0;
	int dstI = 0;
	int dstJ = 0;
	bool legalMove = false;
	bool isCheckMate = false;

	//the first set of the double for loop runs on the whole 2d array board, to get the source piece to move
	for (srcI = 0; srcI < FILE_AND_RANK && ans; srcI++)
	{
		for (srcJ = 0; srcJ < FILE_AND_RANK && ans; srcJ++)
		{
			if (this->_board[srcI][srcJ] != nullptr && this->_board[srcI][srcJ]->getColor() == turn)  //checks if the current piece on the board is in the same color as chesses king
			{
				//the second set of the double for loops runs oon the whole 2d array board, to get the destination place to move the source piece
				for (dstI = 0; dstI < FILE_AND_RANK && ans; dstI++)
				{
					for (dstJ = 0; dstJ < FILE_AND_RANK && ans; dstJ++)
					{
						//checks if the created movement is legal according to the source piece
						legalMove = this->_board[srcI][srcJ]->legalMove(Point(dstI, dstJ), this->_board);
						if (legalMove && dstI != srcI && dstJ != srcJ)
						{
							//if the movement is legal, we check if it prevents from the given king color from being in chess, by using the funciton checkChess with the created movement
							isCheckMate = checkChess(Point(srcI, srcJ), Point(dstI, dstJ), turn);
							if (!isCheckMate)
							{
								ans = false;  //if a movement that gets the king off chess has been found
							}
						}
					}
				}
			}
		}
	}

	return ans;
}


/*
The function finds the coorinates of the given turn king and puts it in the given paramters x and y
Input: the created temp board which contains all the pieces on the board by their type, the turn we want to fing its king, and two paramters to put the king coordinates in
*/
void Board::getKing(Piece* tempBoard[][FILE_AND_RANK], const bool turn, int& x, int& y)
{
	int i = 0;
	int j = 0;
	char currPiece = 'a';
	bool islow = false;

	for (i = 0; i < FILE_AND_RANK; i++)
	{
		for (j = 0; j < FILE_AND_RANK; j++)
		{
			if (tempBoard[i][j] != nullptr)  //checks if the current piece isnt empty to prevent any errors
			{
				currPiece = tempBoard[i][j]->getType();  //gets the current piece type
				islow = islower(currPiece);  //checks if the curret piece is white or black (if lower char - true -> black)  

				if ((islow == turn) && (tolower(currPiece) == 'k'))  //check if the current piece is a king on the right side(black or white)
				{
					//puts the coordinate of the king inside the given paramters
					x = i;
					y = j;
				}
			}
		}
	}
}


/*
The function gets the movement and creats it on the created board
Input: the movment - Point class src which symbolizes the piece the user want to move, Point class dst which symbolizes the place the user wants to put the src piece in
*/
void Board::movePiece(const Point& src, const Point& dst)
{
	this->_board[src.getX()][src.getY()]->resetFirst();  //resets the _first variable because a first move of some piece has accured

	this->_board[src.getX()][src.getY()]->set_Pos(dst);  //changes the current position of the moving piece in its class to its new position according to the move
	delete this->_board[dst.getX()][dst.getY()];  //deletes the dst pos piece

	this->_board[dst.getX()][dst.getY()] = this->_board[src.getX()][src.getY()];  //moves the src pos to the dst pos
	this->_board[src.getX()][src.getY()] = nullptr;
}


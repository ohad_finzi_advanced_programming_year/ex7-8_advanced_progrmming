#include "Knight.h"


/*
The function constructs the current Knight class according to the given elements
Input: char which symbolizes the curent piece type and color and a string which symbolizes the current piece position on the board
*/
Knight::Knight(const char type, const Point& pos):
	Piece(type, pos)
{
}


/*
The function deconstructs the current Knight class
*/
Knight::~Knight()
{
	//no dynamic memory alocated in this class
}


/*
The function overrides the pure abstract function from Piece class.
The function checks if the given string movement is valid according to the current piece.
Input: the wanted movement of the current piece
Output: true if the move is valid according to the current piece and false if not
*/
bool Knight::legalMove(const Point& dstPos, Piece* board[FILE_AND_RANK][FILE_AND_RANK]) const
{
	int srcI = _pos.getX();
	int srcJ = _pos.getY();
	int dstI = dstPos.getX();
	int dstJ = dstPos.getY();

	return ((abs(srcI - dstI) == 2 && abs(srcJ - dstJ) == 1) || (abs(srcJ - dstJ) == 2 && abs(srcI - dstI) == 1)) ? true : false;  //checks if the movement is valid for the Knight piece (the knight can move 2 step forward and then one step diagonaly to the side)
}


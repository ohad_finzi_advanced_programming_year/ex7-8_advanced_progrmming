#pragma once

#include "Queen.h"


class Rook : public Queen
{

public:
	//constructor and deconstructor
	Rook(const char type, const Point& pos) :
		Queen(type, pos, false, true, false)  //the Rook can move in striaght lines and he is not limited
	{
	}
	virtual ~Rook()
	{
		//no dynamic memory alocated in this class
	}

};


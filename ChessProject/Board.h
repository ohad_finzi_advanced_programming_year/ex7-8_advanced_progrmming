#include <iostream>
#include "Point.h"
#include "Piece.h"
#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"


#define FILE_AND_RANK 8

enum VALIDMOVE
{
	VALID,
	CHESSVALID,
	SRCINVALID,
	DSTINVALID,
	CHESSINVALID,
	INDEXINVALID,
	MOVEINVALID,
	WRONGINDEX,
	CHECKMATE
};


class Board
{

private:
	//fields
	Piece* _board[FILE_AND_RANK][FILE_AND_RANK] = { 0 };

public:
	//constructor and deconstructor
	Board(const std::string board);
	~Board();

	//movement related
	int checkMove(const Point& src, const Point& dst, const bool currTurn);
	bool checkChess(const Point src, const Point dst, const bool turn);
	bool checkMate(const bool turn);
	void getKing(Piece* tempBoard[][FILE_AND_RANK], const bool turn, int& x, int& y);
	void movePiece(const Point& src, const Point& dst);

};


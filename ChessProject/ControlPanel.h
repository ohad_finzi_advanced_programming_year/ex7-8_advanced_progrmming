#include <iostream>
#include "ConnectEngine.h"
#include "Board.h"


#define STARTING_BOARD "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"
#define STARTING_TURN_POS 64
#define LEGAL_MOVE (ans == 0 || ans == 1 || ans == 8)  //the move is valid only if and is 0 or 1 or 8


class ControlPanel
{

private:
	//fields
	Board _gameBoard;
	ConnectEngine _engine;
	bool _currTurn;  //white's turn -> false || black's turn -> true

public:
	//constructor and deconstructor
	ControlPanel(const std::string& startingBoard = STARTING_BOARD);
	~ControlPanel();

	void runGame();

};


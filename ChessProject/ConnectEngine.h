#pragma once

#include "Pipe.h"
#include <iostream>
#include <thread>


#define MAX_MSG_CHARS 1024


class ConnectEngine
{

private:
	//fields
	Pipe _pipe;

	//connects to the engine
	void connect();

public:
	//constructor and deconstructor
	ConnectEngine(const std::string& startingBoard);
	~ConnectEngine();

	//communications functions
	void sendMsg(const std::string& msg);
	std::string receiveMsg();

};


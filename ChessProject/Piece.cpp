#include "Piece.h"


/*
The function resets the _first variable
*/
void Piece::resetFirst()
{
	this->_first = false;
}


/*
The function constructs the current Piece class according to the given elemnts
Input: char which symbolizes the curent piece type and color and a string which symbolizes the current piece position on the board
*/
Piece::Piece(const char& type, const Point& pos, bool first):
	_pos(pos), _type(type)
{
	this->_color = islower(type);  //lowercase -> true = black, uppercase -> false = white
	this->_first = first;  //the first value on default is false becuase no other piece except the pawn uses it 
}


/*
The function deconstructs the current Piece class 
*/
Piece::~Piece()
{
	//no dynamic memory alocated in this class
}


/*
The function returns the current piece position on the board
*/
Point Piece::getPos() const
{
	return this->_pos;
}


/*
The function sets a new board position for the current Piece class
Input: the new position string
*/
void Piece::set_Pos(const Point& pos)
{
	this->_pos = pos;
}


/*
The function returns the current Piece color
*/
bool Piece::getColor() const
{
	return this->_color;
}


/*
The function returns the current Piece type
*/
char Piece::getType() const
{
	return this->_type;
}


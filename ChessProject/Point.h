#pragma once

#include <iostream>


#define FIRST_LOWER 97
#define FIRST_NUMBER 49


class Point
{
private:
	//fields
	int _x;
	int _y;

public:
	//constructors and deconstructor
	Point();
	Point(const int x, const int y);
	Point(const std::string& pos);
	Point(const Point& p);
	~Point();

	//getters and setters
	int getX() const;
	int getY() const;
	void setX(const int x);
	void setY(const int y);
	void setPos(const std::string& pos);
	
	int calcDis(const Point& p) const;

	//operator overriding
	Point& operator+=(const Point& p);
	Point& operator=(const Point& p);
	bool operator==(const Point& p) const;

};


#pragma once

#include "Piece.h"


class Queen : public Piece
{

private:
	//fields
	bool _diagonal;
	bool _straight;
	bool _limit;

public:

	Queen(const char type, const Point& pos, const bool diagonal = true, const bool striaght = true, const bool limit = false);
	virtual ~Queen();

	//overrideing the pure abstract function from the Piece class
	virtual bool legalMove(const Point& dstPos,  Piece* board[][FILE_AND_RANK]) const;
	
	//checks move function
	bool checkDiagonal(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const;
	bool checkStraight(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const;

};


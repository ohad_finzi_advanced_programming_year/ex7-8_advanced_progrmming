#include "ControlPanel.h"


/*
The function constructs the current ControlPanel class by creating the Board class, the ConnectEngine class and getting the first player to player
*/
ControlPanel::ControlPanel(const std::string& startingBoard):
	_gameBoard(startingBoard), _engine(startingBoard)
{
	//extracts the 65 char from the board which symbolizes the 
	this->_currTurn = startingBoard[STARTING_TURN_POS] != '0';  //true for this statement symbolize for black, false will symbolize for white
}


/*
The function deconstructs the current ControlPanel class
*/
ControlPanel::~ControlPanel()
{
	//no dynamic memory alocated in this class
}


/*
The function runs a loop which plays the game by sending msgs to the server and receiving new commands from the server
*/
void ControlPanel::runGame()
{
	std::string msgFromGraphics = "";
	std::string msgToGraphics = "";
	int ans = 0;
	Point src = Point();
	Point dst = Point();

	while (ans != 8)  //runs until there is no checkmate on the board from either side
	{
		msgFromGraphics = _engine.receiveMsg();

		Point src(msgFromGraphics.substr(0, 2));  //extracts the src position
		Point dst(msgFromGraphics.substr(2, 3));  //extracts the dst position

		ans = _gameBoard.checkMove(src, dst, _currTurn);  //checks the given movement
		msgToGraphics = std::to_string(ans);  //converts the ans variable (which symbolizes if the given movement is valid)
		_engine.sendMsg(msgToGraphics);  //sends the answer back to the engine

		if (LEGAL_MOVE)
		{
			_currTurn = !_currTurn;  //switches sides turn because the move is valid
		}
	}
}


#include "King.h"


/*
The function constructs the current King class according to the given elements
Input: the type of the piece (k or K), and its position on the board
*/
King::King(const char type, const Point& pos) :
	Queen(type, pos, true, true, true)  //the King can move to all sides (in diagonal and striaght lines) and he is limited
{
}


/*
The function deconstructs the current King class
*/
King::~King()
{
	//no dynamic memory alocated in this class
}


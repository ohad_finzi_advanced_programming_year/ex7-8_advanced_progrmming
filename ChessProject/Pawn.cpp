#include "Pawn.h"


/*
The function constructs the current Pawn class according to the given parameters
Input: the type of the piece (k or K), and its position on the board
*/
Pawn::Pawn(const char type, const Point pos):
	Piece(type, pos, true)  //the _first variable indicates on the pawn piece special ablity which allows him to to make two steps in his first move
{
}


/*
The function deconstructs the current Pawn class
*/
Pawn::~Pawn()
{
	//no dynamic memory alocated in this class
}


/*
The function overrides the pure abstract function from Piece class.
The function checks if the given string movement is valid according to the current piece.
Input: the wanted movement of the current piece
Output: true if the move is valid according to the current piece and false if not
*/
bool Pawn::legalMove(const Point& dstPos, Piece* board[][FILE_AND_RANK]) const
{
	bool ans = false;
	int srcI = _pos.getX();
	int srcJ = _pos.getY();
	int dstI = dstPos.getX();
	int dstJ = dstPos.getY();
	bool dstColor = false;

	(this->_color) ? srcI-- : srcI++;  //moves the current position of the pawn one step forward or backward according to the current color (black - backward, white forward)
	if (board[dstI][dstJ] == nullptr)  //checks if the distance position is empty
	{
		if ((srcI < FILE_AND_RANK) && (srcI == dstI) && (srcJ == dstJ))  //checks if one move forward is valid. the move is valid if the dst pos is empty
		{
			ans = true;
		}
		else
		{
			(this->_color) ? srcI-- : srcI++;  //moves the current position of the pawn one step forward or backward according to the current color (black - backward, white forward)
			if ((srcI < FILE_AND_RANK) && (srcI == dstI) && (srcJ == dstJ) && this->_first)  //checks if two move forward is valid. the move is valid if the dst pos is empty
			{
				ans = true;
			}
		}
	}
	else  //if the distance position has a player
	{
		dstColor = board[dstI][dstJ]->getColor();  //gets the dst position piece color

		if ((((srcJ - 1) >= 0) && ((srcJ - 1) == dstJ) && (srcI == dstI) && (dstColor != this->_color)) ||  //checks if one step diagonaly to the left is valid. the move is valid if the dst pos has a piece of the other side
			(((srcJ + 1) < FILE_AND_RANK) && ((srcJ + 1) == dstJ) && (srcI == dstI) && (dstColor != this->_color)))  //checks if one step diagonaly to the right is valid. the move is valid if the dst pos has a piece of the other side
		{
			ans = true;
		}
	}

	return ans;
}


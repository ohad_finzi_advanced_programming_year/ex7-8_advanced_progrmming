#pragma once

#include "Piece.h"


class Knight : public Piece
{

public:
	//constructor and deconstructor
	Knight(const char type, const Point& pos);
	virtual ~Knight();

	//overrides the pure abstract function move from Piece class
	virtual bool legalMove(const Point& dstPos, Piece* board[FILE_AND_RANK][FILE_AND_RANK]) const;

};

